# Stock Price Chart Web App

This is a web application that displays real-time stock price charts using React and FastAPI. It allows users to select a stock symbol and view the corresponding price chart for the past day
## Features

- Real-time stock price data: The application fetches stock price data from the Yahoo Finance API using the `yfinance` library.
- Interactive chart: The stock price chart is rendered using the Recharts library, providing an interactive and visually appealing representation of the data.
- Stock symbol selection: Users can choose from a predefined set of stock symbols (META, AMD, AAPL) to view the corresponding price chart.
- Responsive design: The application is designed to be responsive and can adapt to different screen sizes.

## Technologies Used

- Frontend:
    - React: A JavaScript library for building user interfaces.
    - Recharts: A powerful charting library built with React and D3.
    - Axios: A promise-based HTTP client for making API requests.
- Backend:
    - FastAPI: A modern, fast, web framework for building APIs with Python.
    - yfinance: A library for retrieving stock market data from Yahoo Finance.

## Prerequisites

- Python 3.7+
- Node.js 12+
- npm (Node Package Manager)

## Installation

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/nicola.bombaci/stock-price-chart.git
    ```

2. Navigate to the project directory:

   ```bash
   cd .\stock-price-chart
   ```

3. Create a virtual environment (optional but recommended):

   ```bash
   cd .\backend
   python -m venv venv
    ```

   Activate the virtual environment:
    - For Windows:

      ```bash
      venv\Scripts\activate
      ```
    - For macOS and Linux

      ```bash
      source venv/bin/activate
      ```

4. Install the backend dependencies:

    ```bash
    pip install -r requirements.txt
   ```

5. Navigate to the frontend directory:

    ```bash
   cd ..\frontend
   ```

6. Install the frontend dependencies:

    ```bash
   npm install
   ```

## Usage

1. Start the backend server:

    ```bash
   cd .\backend\app
   ```
   ```bash
   uvicorn main:app --reload
   ```

2. In a separate terminal, start the frontend development server:
    ```bash
   cd .\frontend
   ```
   ```bash
   npm start
   ```

3. Open your web browser and visit http://localhost:3000 to access the application.

4. Select a stock symbol from the provided options (META, AMD, AAPL) to view the corresponding stock price chart.


## Configuration

### Backend
- The FastAPI server runs on http://localhost:8000 by default. If you need to change the host or port, modify the uvicorn.run() command in the backend/main.py file.
- To change the default stock symbol, modify the symbol parameter in the get_stock_price function in the backend code (main.py).

### Frontend
- The React application is configured to proxy API requests to http://localhost:8000. If you change the backend server's host or port, update the proxy field in the frontend/package.json file accordingly.


## API

### `stock_price`

The endpoint is a GET API that retrieves stock price data for a specified stock symbol.

#### Input

- `symbol` (optional): The stock symbol for which you want to retrieve the price data. The default value is "META".

Example request: `GET /stock_price?symbol=AAPL`

#### Output

The API returns a JSON object containing the stock price data for the specified symbol. The data is structured as an array of objects, where each object represents a data point of the stock price.

Example response:
```json
{
  "data": [
    {
      "Symbol": "AAPL",
      "Datetime": "2023-06-08T09:30:00-04:00",
      "Open": 180.1,
      "High": 180.2,
      "Low": 179.9,
      "Close": 180.0,
      "Volume": 1000000
    },
    ...
  ]
}
```