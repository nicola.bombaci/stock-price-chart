from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from endpoints import router

app = FastAPI()
app.add_middleware(
    CORSMiddleware,       # handle Cross-Origin Resource Sharing
    allow_origins=["*"],  # allows requests from any origin
    allow_methods=["*"],  # allows any HTTP method
    allow_headers=["*"],  # allows any headers in the HTTP requests
)

app.include_router(router)

# checks if the script is being run directly (not imported as a module)
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="localhost", port=8000)
