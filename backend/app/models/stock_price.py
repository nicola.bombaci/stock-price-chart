from datetime import datetime
from pydantic import BaseModel


class StockPrice(BaseModel):
    Symbol: str
    Datetime: datetime
    Open: float
    High: float
    Low: float
    Close: float
    Volume: int