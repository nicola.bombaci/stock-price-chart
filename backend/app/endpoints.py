from fastapi import APIRouter
import yfinance as yf
from models.stock_price import StockPrice

router = APIRouter()


"""
GET: /stock_price
Fetches and returns the stock price data for a given symbol at 1-minute intervals 
for the last trading day.

:arg
symbol (str): The stock symbol to fetch data for. Defaults to "META".

:returns
dict: A dictionary containing the stock price data, where the key is "data" 
      and the value is a list of StockPrice instances, each representing 
      a snapshot of the stock's price at a specific time.
"""
@router.get("/stock_price")
def get_stock_price(symbol: str = "META") -> dict:
    stock_data = yf.download(symbol, interval="1m", period="1d")  # fetch data at 1 minute intervals
    stock_data = stock_data.reset_index()  # reset the index of the DataFrame, making the date-time index a column

    stock_prices = []  # output data
    for index, row in stock_data.iterrows():
        # creates an instance of StockPrice
        stock_price = StockPrice(
            Symbol=symbol,
            Datetime=index,
            Open=row["Open"],
            High=row["High"],
            Low=row["Low"],
            Close=row["Close"],
            Volume=row["Volume"]
        )
        stock_prices.append(stock_price)

    return {"data": stock_prices}
