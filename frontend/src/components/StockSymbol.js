import React from 'react';

const StockSymbol = ({ selectedSymbol, onSymbolChange }) => {
    return (
        <div>
            <button onClick={() => onSymbolChange('META')} disabled={selectedSymbol === 'META'}>META</button>
            <button onClick={() => onSymbolChange('AMD')} disabled={selectedSymbol === 'AMD'}>AMD</button>
            <button onClick={() => onSymbolChange('AAPL')} disabled={selectedSymbol === 'AAPL'}>AAPL</button>
        </div>
    );
};

export default StockSymbol;
