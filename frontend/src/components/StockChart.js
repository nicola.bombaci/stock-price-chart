import React, { useEffect, useState } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';
import { format  } from 'date-fns';
import axios from 'axios';
import './StockChart.css';

const StockChart = ({ symbol }) => {
    // Definisce uno stato 'stockData' inizializzato come un array vuoto e una funzione 'setStockData' per aggiornarlo
    const [stockData, setStockData] = useState([]);
    const [lastUpdated, setLastUpdated] = useState(null);

    const fetchData = async () => {
        try {
            // GET request on specified symbol
            const result = await axios.get(`http://localhost:8000/stock_price?symbol=${symbol}`);
            setStockData(result.data.data); // update stockData
            setLastUpdated(new Date()); // update lastUpdate

        } catch (error) {
            console.error('Error fetching stock data:', error);
        }
    };

    // hook executed once the components is rendered
    useEffect(() => {
        fetchData();
        const interval = setInterval(fetchData, 60000); // run fetchData every 60 seconds
        return () => clearInterval(interval); // remove interval when components is unmounted

    }, [symbol]);

    // Calculate minimum and maximum price values
    const minPrice = Math.min(...stockData.map(data => data.Close));
    const maxPrice = Math.max(...stockData.map(data => data.Close));

    // Format YAxis to 2nd decimal value
    const formatYAxisTick = (value) => {
        return value.toFixed(2);
    };

    // Format XAxis using a simple format
    const formatXAxisTick = (tickItem) => {
        if (tickItem === stockData[0]?.Datetime) {
            return '1 day ago';
        } else if (tickItem === stockData[stockData.length - 1]?.Datetime) {
            return 'now';
        }
        return '';
    };

    return (
        <div>
            <h2>{symbol}</h2>
            <LineChart className="LineChart-box" width={800} height={500} data={stockData}>
                <XAxis
                    dataKey="Datetime"
                    tickFormatter={formatXAxisTick}
                    tickMargin={10}
                    ticks={[stockData[0]?.Datetime, stockData[stockData.length - 1]?.Datetime]}
                />
                <YAxis
                    domain={[minPrice, maxPrice]}
                    tickCount={10}
                    tickMargin={10}
                    tickFormatter={formatYAxisTick}
                />
                <CartesianGrid strokeDasharray="3 3" />
                <Tooltip />
                <Line
                    type="monotone"
                    dataKey="Close"
                    stroke="#8884d8"
                    dot={false}
                    activeDot={false}
                />
            </LineChart>
            {lastUpdated && (
                <div className="LastUpdate-box">
                    Last updated: {format(lastUpdated, 'yyyy-MM-dd HH:mm:ss')}
                </div>
            )}
        </div>
    );
};

export default StockChart;
