import './App.css';
import StockChart from "./components/StockChart";
import StockSymbol from "./components/StockSymbol";
import {useState} from "react";

function App() {
    const [selectedSymbol, setSelectedSymbol] = useState('META');

    const handleSymbolChange = (symbol) => {
        setSelectedSymbol(symbol);
    };

    return (
        <div className="App">
            <header className="App-header">
                <h1>Stock Price Chart Web App</h1>
            </header>
            <main>
                <StockSymbol selectedSymbol={selectedSymbol} onSymbolChange={handleSymbolChange} />
                <StockChart symbol={selectedSymbol} />
            </main>
            <footer>
                <p>&copy; 2023 Stock Price Chart Web App. All rights reserved.</p>
            </footer>
        </div>
    );
}

export default App;
